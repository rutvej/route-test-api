install python virtual environment

                                $ pip install virtualenv

create a python virtual environments

for windows use this command

               $ python -m venv virtualenv_name
                
               $ virtualenv_name\Scripts\activate.bat

for linux and mac use this command

                $ python3 -m venv virtualenv_name
                
                $ source virtualenv_name/bin/activate

install the required lib

                $ pip install -r requirements.txt

Run the main file
                
                $ python main.py

For testing

'/item' route is a post method which takes application/json param named item which are [book, pen, folder, bag]
in return it gives the id status and the name of the item current insertion (please not item which are not in the list will be invalide and will rasie an error )


'/delayvalue' route is a get method which takes query params name 'delay_value' and data type is integer which return the time duration taken to excute 5 concurrent request on https://httpbin.org/delay/{delay_value} where delay_value is user provided




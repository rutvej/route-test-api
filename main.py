from flask import *
from flask_sqlalchemy import SQLAlchemy
from marshmallow import Schema, fields, ValidationError, pre_load
from threading import Thread
import requests
import time
app = Flask(__name__)
app.config.from_pyfile('config.py')
db = SQLAlchemy(app)


class items(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    item = db.Column(db.String(20),nullable=False)
    status = db.Column(db.String(20),nullable=False)

def validate(data):
    if data not in ['book', 'pen', 'folder', 'bag']:
        raise ValidationError('invalid item')
class itemsSchema(Schema):
    id = fields.Integer()
    item = fields.String(required=True,validate=validate)
    status = fields.String()

item_schema = itemsSchema()
@app.route('/item' ,methods = ['POST'])
def inputt():
    name = request.get_json()
    if not name:
        return {"message": "No input data provided"}, 400
    if 'item' in name.keys():
        try:
            data = item_schema.load(name)
        except ValidationError as err:
            return err.messages,422
        new_item = items(item=data['item'], status="pending")
        db.session.add(new_item)
        db.session.commit()
        return jsonify({'id':new_item.id,'item':new_item.item,'status':new_item.status}),200
    return {"message": "Wrong input data provided"}, 400

@app.route('/delayvalue' ,methods = ['GET'])
def val():
    value = request.args.get('delay_value',type=int)
    if not value:
        return 'PLease provide interger value only  or Use the correct param /delayvalue?delay_value=1',400
    @timetaken
    def get(val):
        url = "https://httpbin.org/delay/{}".format(val)
        t1 = Thread(target=requests.request,args=('GET',url))
        t2 = Thread(target=requests.request,args=('GET',url))
        t3 = Thread(target=requests.request,args=('GET',url))
        t4 = Thread(target=requests.request,args=('GET',url))
        t5 = Thread(target=requests.request,args=('GET',url))
        t1.start()
        t2.start()
        t3.start()
        t4.start()
        t5.start()
        t1.join()
        t2.join()
        t3.join()
        t4.join()
        t3.join()


    Responsetime = get(int(value))
    return jsonify({"time_taken":Responsetime}),200

def timetaken(func):
    def timeit(val):
        timestart = time.time()
        Response = func(val)
        timeend = time.time()
        return timeend-timestart
    return timeit

if __name__ == "__main__":
    db.create_all()
    app.run()

